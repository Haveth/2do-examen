# 2do Examen

José Luis Reyes Mauricio 
28904591
PD = Observer
Observer es un patrón de diseño de software que define una dependencia del tipo uno a muchos entre objetos, de manera que cuando uno de los objetos cambia su estado, notifica este cambio a todos los dependientes.