# 2do Examen

José Luis Reyes Mauricio 
28904591
PD = Factory
Factory Method permite la creación de objetos de un subtipo determinado a través de una clase Factory. 
Esto es especialmente útil cuando no sabemos, en tiempo de diseño, el subtipo que vamos a utilizar
o cuando queremos delegar la lógica de creación de los objetos a una clase Factory.
