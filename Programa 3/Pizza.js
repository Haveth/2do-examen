function Pizza(){       
    this.crearPizza = function(type){
        if (type === "PizzaEspecial") {
            return PizzaEspecial();
        }else if (type === "PizzaHawaiana"){
            return PizzaHawaiana();
        }else if (type === "PizzaItaliana"){
            return PizzaItaliana();
        }else if (type === "Pastel"){
            return PizzaItaliana();
        };
    }
 
    function Pizza(ing,precio){
        this.Ingredientes =ing;
        this.Precio = precio;
    }
 
    function PizzaEspecial(){
        return new Pizza(["jamon","queso","salsa","carne"],
                    150);
    }
 
    function PizzaHawaiana(){
        return new Pizza(["tomate","queso","salsa"],
            150);
    }

    function PizzaItaliana(){
        return new Pizza(["tomate","queso","salsa","salchica","albaca"],
            150);
    }

    function Pastel(){
        return new Pizza(["harina","leche","betun"],
            150);
    }
}
 
var factory = new Pizza();
var pizza = factory.crearPizza("Pastel");
 
console.log(pizza);